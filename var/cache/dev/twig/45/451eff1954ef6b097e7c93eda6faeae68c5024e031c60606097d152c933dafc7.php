<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/trouver/index.html.twig */
class __TwigTemplate_f1e2ad5716cdfdee4714fc546d4c775ffa09ddac881aa482296b8d82bba2a62d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/trouver/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/trouver/index.html.twig"));

        $this->parent = $this->loadTemplate("admin.html.twig", "admin/trouver/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Ou nous trouver!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
<h5 class=\"titre-categorie\">Ou nous trouver</h5>

</div>
<div class=\"container\">
<a href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_trouver_add");
        echo "\" type=\"button\"
    class=\"btn btn-success\">Ajouter une adresse</a>
</div>
";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["trouver"]) || array_key_exists("trouver", $context) ? $context["trouver"] : (function () { throw new RuntimeError('Variable "trouver" does not exist.', 14, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["trouvers"]) {
            // line 15
            echo "<div class=\"container2\">
<div class=\"container-card\">
<div class=\"card \" style=\"width: 14rem;\">
    <h5 class=\"card-title\">";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["trouvers"], "ville", [], "any", false, false, false, 18), "html", null, true);
            echo "</h5>
    <p class=\"card-text\">Le ";
            // line 19
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["trouvers"], "jour", [], "any", false, false, false, 19), "html", null, true);
            echo "</p>
    <p class=\"card-text\">de ";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["trouvers"], "debut", [], "any", false, false, false, 20), "html", null, true);
            echo " à ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["trouvers"], "fin", [], "any", false, false, false, 20), "html", null, true);
            echo "</p>
    <img src=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("/uploads/" . twig_get_attribute($this->env, $this->source, $context["trouvers"], "image", [], "any", false, false, false, 21))), "html", null, true);
            echo "\" alt=\"Image\" class=\"card-img-top\" alt=\"...\">
    <div class=\"card-body\">
        <h5 class=\"card-title\">";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["trouvers"], "adresse", [], "any", false, false, false, 23), "html", null, true);
            echo "</h5>
        <a class=\"btn btn-primary\" href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_trouver_edit", ["id" => twig_get_attribute($this->env, $this->source, $context["trouvers"], "id", [], "any", false, false, false, 24)]), "html", null, true);
            echo "\">Editer<i
                class=\"fa fa-edit\"></i></a>
        <form action=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_trouver_delete", ["id" => twig_get_attribute($this->env, $this->source, $context["trouvers"], "id", [], "any", false, false, false, 26)]), "html", null, true);
            echo "\" method=\"post\" class=\"d-inline\">
            <button class=\"btn btn-danger\">Supprimer<i class=\"fa fa-trash\"></i></button>
            <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
        </form>
    </div>
</div>
</div>
</div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trouvers'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/trouver/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 35,  138 => 26,  133 => 24,  129 => 23,  124 => 21,  118 => 20,  114 => 19,  110 => 18,  105 => 15,  101 => 14,  95 => 11,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.html.twig' %}

{% block title %}Ou nous trouver!{% endblock %}

{% block body %}
<div class=\"container\">
<h5 class=\"titre-categorie\">Ou nous trouver</h5>

</div>
<div class=\"container\">
<a href=\"{{ path('admin_trouver_add')}}\" type=\"button\"
    class=\"btn btn-success\">Ajouter une adresse</a>
</div>
{% for trouvers in trouver %}
<div class=\"container2\">
<div class=\"container-card\">
<div class=\"card \" style=\"width: 14rem;\">
    <h5 class=\"card-title\">{{trouvers.ville}}</h5>
    <p class=\"card-text\">Le {{trouvers.jour}}</p>
    <p class=\"card-text\">de {{trouvers.debut}} à {{trouvers.fin}}</p>
    <img src=\"{{ asset('/uploads/'~trouvers.image )}}\" alt=\"Image\" class=\"card-img-top\" alt=\"...\">
    <div class=\"card-body\">
        <h5 class=\"card-title\">{{trouvers.adresse}}</h5>
        <a class=\"btn btn-primary\" href=\"{{ path('admin_trouver_edit', {id: trouvers.id}) }}\">Editer<i
                class=\"fa fa-edit\"></i></a>
        <form action=\"{{ path('admin_trouver_delete', {id: trouvers.id}) }}\" method=\"post\" class=\"d-inline\">
            <button class=\"btn btn-danger\">Supprimer<i class=\"fa fa-trash\"></i></button>
            <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
        </form>
    </div>
</div>
</div>
</div>
{% endfor %}

{% endblock %}
", "admin/trouver/index.html.twig", "/var/www/html/foodtruck/templates/admin/trouver/index.html.twig");
    }
}
